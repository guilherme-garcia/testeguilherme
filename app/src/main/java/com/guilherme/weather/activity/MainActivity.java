package com.guilherme.weather.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.guilherme.weather.R;
import com.guilherme.weather.model.Weather;
import com.guilherme.weather.webservice.Conexao;
import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity {

    private EditText et_city;
    private Button btn_sel;
    private TextView tv_temp;
    private TextView tv_city;
    private TextView tv_desc;
    private ProgressBar bar;
    private ImageView view;
    private TextView tv_rise;
    private TextView tv_set;
    private TextView tv_tempMax;
    private TextView tv_tempMin;
    private TextView tv_humidade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_temp = findViewById(R.id.txt_temp);
        tv_tempMax = findViewById(R.id.txt_tempMax);
        tv_tempMin = findViewById(R.id.txt_tempMin);
        tv_city = findViewById(R.id.txt_city);
        tv_desc = findViewById(R.id.txt_desc);
        et_city = findViewById(R.id.etxt_city);
        btn_sel = findViewById(R.id.btn_city);
        bar = findViewById(R.id.progress);
        tv_rise = findViewById(R.id.txt_rise);
        tv_set = findViewById(R.id.txt_set);
        tv_humidade = findViewById(R.id.txt_humidade);
        view = findViewById(R.id.image);


        btn_sel.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {

                String city = et_city.getText().toString();

                //Executa a task
                JSONTask task = new JSONTask();
                task.execute(new String[]{city});

                bar.setVisibility(View.VISIBLE);

                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) btn_sel.getLayoutParams();
                params.bottomToBottom = -1;
                btn_sel.requestLayout();

                ConstraintLayout.LayoutParams params1 = (ConstraintLayout.LayoutParams) et_city.getLayoutParams();
                params1.bottomToBottom = -1;
                et_city.requestLayout();

            }

        });

    }

    // Classe assincrona para não interromper a UI
    private class JSONTask extends AsyncTask<String, Void, Weather>{

        @Override
        protected Weather doInBackground(String... strings) {

            //Objeto para conter os dados do JSON
            Weather weather = new Weather();
            //Inicia a conexão
            String data = (new Conexao().getWeatherData(strings[0]));


            try {
                //Cria os objetosJSON e insere os dados em variáveis
                JSONObject jsonObject = new JSONObject(data);
                JSONObject main = jsonObject.getJSONObject("main");
                JSONObject coord = jsonObject.getJSONObject("coord");
                JSONArray jsonArray = jsonObject.getJSONArray("weather");
                JSONObject object = jsonArray.getJSONObject(0);
                String cidade = jsonObject.getString("name");
                double lon = coord.getDouble("lon");
                double lat = coord.getDouble("lat");
                String description = object.getString("description");
                double temp = main.getDouble("temp");
                String icon = object.getString("icon");
                double tempMax = main.getDouble("temp_max");
                double tempMin = main.getDouble("temp_min");
                String humidade = valueOf(main.getDouble("humidity"));

                // conversão de unidades
                double tempConvert = Math.round((temp-273));
                String tempString = valueOf(tempConvert);

                double tempMaxConvert = Math.round((tempMax-273));
                String tempMaxString = valueOf(tempMaxConvert);
                double tempMinConvert = Math.round((tempMin-273));
                String tempMinString = valueOf(tempMinConvert);

                //inserindo dados na instancia de weather
                weather.localizacao.setCidade(cidade);
                weather.temp.setImagem(icon);

                Log.i("Teste", weather.localizacao.getCidade());

                //Faz a conexão para obter a imagem passando a string com o codigo do icone
                weather.imagemData = new Conexao().getImage(icon);
                weather.temp.setTempAtual(tempString);
                weather.temp.setTempMax(tempMaxString);
                weather.temp.setTempMin(tempMinString);
                weather.temp.setDescricao(description);
                weather.temp.setHumidade(humidade);


                // coordenadas usando biblioteca externa
                Location location = new Location(lon, lat);
                SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, "America/Sao_Paulo");
                String officialSunrise = calculator.getOfficialSunriseForDate(Calendar.getInstance());
                String officialSunset = calculator.getOfficialSunsetForDate(Calendar.getInstance());

                weather.localizacao.setSunrise(officialSunrise);
                weather.localizacao.setSunset(officialSunset);

                Log.i("RISE", officialSunrise);
                Log.i("SET", officialSunset);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return weather;
        }

        @Override
        protected void onPostExecute(Weather weather) {
            super.onPostExecute(weather);


            Log.i("Teste", weather.localizacao.getCidade());

            //Recebe o weather como resultado e confere se esta ok
            //Insere o elemento
            if (weather.imagemData != null && weather.imagemData.length > 0){
                Bitmap bitmap = BitmapFactory.decodeByteArray(weather.imagemData, 0, weather.imagemData.length);
                view.setImageBitmap(bitmap);

            }

                // Insere os dados nos elementos
                tv_city.setText(weather.localizacao.getCidade());
                tv_temp.setText( weather.temp.getTempAtual() + "°");
                tv_tempMax.setText(weather.temp.getTempMax()+ "°");
                tv_tempMin.setText(weather.temp.getTempMin()+ "°");
                tv_desc.setText(weather.temp.getDescricao());
                tv_humidade.setText(weather.temp.getHumidade() + "%");
                tv_rise.setText(weather.localizacao.getSunrise());
                tv_set.setText(weather.localizacao.getSunset());
                bar.setVisibility(View.GONE);
                tv_tempMax.setVisibility(View.VISIBLE);
                tv_tempMin.setVisibility(View.VISIBLE);
                tv_city.setVisibility(View.VISIBLE);
                tv_desc.setVisibility(View.VISIBLE);
                tv_rise.setVisibility(View.VISIBLE);
                tv_set.setVisibility(View.VISIBLE);



        }
    }



}
