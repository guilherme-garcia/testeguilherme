package com.guilherme.weather.model;

import java.io.Serializable;

public class Weather implements Serializable {

     public Localizacao localizacao = new Localizacao();

     public  Temperatura temp = new Temperatura();

     public byte[] imagemData;

    public class Temperatura {

        private String tempMin;
        private String tempMax;
        private String tempAtual;
        private String descricao;
        private String imagem;
        private String humidade;

        public String getTempMin() {
            return tempMin;
        }

        public void setTempMin(String tempMin) {
            this.tempMin = tempMin;
        }

        public String getTempMax() {
            return tempMax;
        }

        public void setTempMax(String tempMax) {
            this.tempMax = tempMax;
        }

        public String getTempAtual() {
            return tempAtual;
        }

        public void setTempAtual(String tempAtual) {
            this.tempAtual = tempAtual;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public String getImagem() {
            return imagem;
        }

        public void setImagem(String imagem) {
            this.imagem = imagem;
        }

        public String getHumidade() {
            return humidade;
        }

        public void setHumidade(String humidade) {
            this.humidade = humidade;
        }
    }
}
